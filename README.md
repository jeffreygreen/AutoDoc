[![License](https://img.shields.io/badge/license-MIT-green.svg?style=flat)](https://github.com/JeffreyGreenII/AutoDoc/blob/master/LICENSE)
# AutoDoc
`AutoDoc` is a lightweight PHP documentation framework for PHP 5.6

## Requirements
* Must be run on a PHP server/CLI of version 5.6
* Composer CLI

## Setup
Clone project

Using a CLI, change directory into the cloned project

Run `composer install` to update project dependencies

Execute the sample code with `php index.php` to see a sample output of `tests` directory included in the project.

## Use
Currently working to build out a CLI interface to accept other flags and generate documentation for any PHP project.

Best way to use this in it's current state is to edit `index.php` to have either the absolute or relative path to your project and run using `php index.php` from a CLI.

Alternatively, include this somewhere in your current project, include the autoloader and run `\AutoDoc\AutoDoc::run()` with the mentioned parameters in `index.php` in order to achieve the best outcome.

### Formats
This tool can currently return the following formats:
* JSON
* XML
* RAW (PHP Printed Array)
* HTML (Coming Soon)

The RAW (PHP Printed Array) is currently unavailable. I will be diagnosing this at a later date as this is not a popular format of result for any developers.

The current state of the XML result format is that namespace keywords whose XML element names should read `NameSpace\Name` has had it's backslashes `\` replaced with periods `.` so instead of reading `<NameSpace\Name>` shall read `<NameSpace.Name>` in order to be XML valid.

HTML is an upcoming/experimental feature I am trying to introduce. It will require the location of a writable directory (either an empty one or an available directory name) and attempt to write out HTML files representative of your projects. 

### Depths
This tool has the current ability to document specific information. It can document only `classes` or `functions`, but can also accept a `verbose` parameter which will document everything it can.

## License
This project is licensed under the terms of the MIT license. See the [LICENSE](LICENSE) file.

> This project is open source under the MIT license, which means you have full access to the source code and can modify it to fit your own needs.