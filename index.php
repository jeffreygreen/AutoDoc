<?php
    require __DIR__ . '/vendor/autoload.php';

    $dir = realpath(__DIR__ . '/tests');
    
    $format = \AutoDoc\ResultFormatter::HTML;

    $depth = \AutoDoc\AutoDoc::DEPTH_VERBOSE;

    echo(\AutoDoc\AutoDoc::run($dir, $format, $depth));

    // INFERRED $dir = '.', meaning the directory this file is located
    // INFERRED $format = \AutoDoc\ResultFormatter::JSON
    // INFERRED $depth = \AutoDoc\AutoDoc::DEPTH_CLASSES
    // echo(\AutoDoc\AutoDoc::run());

    // $dir = '/path/to/dir/';
    // $formats = [\AutoDoc\ResultFormatter::JSON,
    //             \AutoDoc\ResultFormatter::XML,
    //             \AutoDoc\ResultFormatter::HTML,
    //             \AutoDoc\ResultFormatter::RAW];

    // $depths = [\AutoDoc\AutoDoc::DEPTH_CLASSES,
    //             \AutoDoc\AutoDoc::DEPTH_FUNCTIONS,
    //             \AutoDoc\AutoDoc::DEPTH_VERBOSE];
