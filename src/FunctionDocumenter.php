<?php
namespace AutoDoc;

class FunctionDocumenter implements Documenter
{
    /**
     * @var \ReflectionFunction[]
     */
    private $functions;

    public function __construct($functions)
    {
        $this->functions = $functions;
    }

    public function document()
    {
        $return = [];

        foreach ($this->functions as $function) {
            $this->getFunctionInfo($return[$function->name], $function);

            $parameters = $function->getParameters();

            foreach ($parameters as $parameter) {
                $this->getFunctionParameterInfo($return[$function->name]["parameters"][$parameter->name], $parameter);
            }
        }

        return $return;
    }

    private function getFunctionInfo(&$array, \ReflectionFunction $reflection)
    {
        $array = [];
        $array["name"] = $reflection->getName();
        $array["file_name"] = $reflection->getFileName();
        $array["start_line"] = $reflection->getStartLine();
        $array["end_line"] = $reflection->getEndLine();
        $array["doc_comment"] = $reflection->getDocComment();
        $array["number_of_parameters"] = $reflection->getNumberOfParameters();
        $array["number_of_required_parameters"] = $reflection->getNumberOfRequiredParameters();
        $array["returns_reference"] = $reflection->returnsReference();
    }

    private function getFunctionParameterInfo(&$array, \ReflectionParameter $reflection)
    {
        $array = [];
        $array["name"] = $reflection->getName();
        $array["position"] = $reflection->getPosition();
        $array["passed_by_reference"] = $reflection->isPassedByReference();
        $array["is_optional"] = $reflection->isOptional();
        $array["is_array"] = $reflection->isArray();
        $array["is_default_value_available"] = $reflection->isDefaultValueAvailable();
        $array["is_variadic"] = $reflection->isVariadic();
        if ($reflection->isDefaultValueAvailable()) {
            $array["default_value"] = $reflection->getDefaultValue();
        }
    }
}
