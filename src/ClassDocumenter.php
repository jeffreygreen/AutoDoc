<?php
namespace AutoDoc;

class ClassDocumenter implements Documenter
{
    /**
     * @var \ReflectionClass[]
     */
    private $classes;

    public function __construct($classes)
    {
        $this->classes = $classes;
    }

    public function document()
    {
        $return = [];

        foreach ($this->classes as $class) {
            $this->getClassInfo($return[$class->name], $class);

            $properties = $class->getProperties();

            foreach ($properties as $property) {
                $this->getClassPropertyInfo($return[$class->name]["properties"][$property->name], $property);
            }

            $methods = $class->getMethods();

            foreach ($methods as $method) {
                $this->getClassMethodInfo($return[$class->name]["methods"][$method->name], $method);

                $parameters = $method->getParameters();

                foreach ($parameters as $parameter) {
                    $this->getClassMethodParameterInfo($return[$class->name]["methods"][$method->name]["parameters"][$parameter->name], $parameter);
                }
            }
        }

        return $return;
    }

    private function getClassInfo(&$array, \ReflectionClass $reflection)
    {
        $array = [];
        $array["name"] = $reflection->getName();
        $array["file_name"] = $reflection->getFileName();
        $array["start_line"] = $reflection->getStartLine();
        $array["end_line"] = $reflection->getEndLine();
        $array["doc_comment"] = $reflection->getDocComment();
        $array['constants'] = $reflection->getConstants();
        $array['constructor'] = $reflection->getConstructor();
        $array['default_properties'] = $reflection->getDefaultProperties();
        $array['extension_name'] = $reflection->getExtensionName();
        $array['interface_names'] = $reflection->getInterfaceNames();
        $array['modifiers'] = $reflection->getModifiers();
        $array['name'] = $reflection->getName();
        $array['namespace_name'] = $reflection->getNamespaceName();
        $array['is_abstract'] = $reflection->isAbstract();
        $array['is_final'] = $reflection->isFinal();
    }

    private function getClassPropertyInfo(&$array, \ReflectionProperty $reflection)
    {
        $array = [];
        $array["name"] = $reflection->getName();
        $array["doc_comment"] = $reflection->getDocComment();
        $array["is_default"] = $reflection->isDefault();
        $array["is_private"] = $reflection->isPrivate();
        $array["is_protected"] = $reflection->isProtected();
        $array["is_public"] = $reflection->isPublic();
        $array["is_static"] = $reflection->isStatic();
    }

    private function getClassMethodInfo(&$array, \ReflectionMethod $reflection)
    {
        $array = [];
        $array["name"] = $reflection->getName();
        $array["doc_comment"] = $reflection->getDocComment();
        $array["number_of_parameters"] = $reflection->getNumberOfParameters();
        $array["number_of_required_parameters"] = $reflection->getNumberOfRequiredParameters();
        $array["returns_reference"] = $reflection->returnsReference();
        $array["is_abstract"] = $reflection->isAbstract();
        $array["is_constructor"] = $reflection->isConstructor();
        $array["is_destructor"] = $reflection->isDestructor();
        $array["is_final"] = $reflection->isFinal();
        $array["is_private"] = $reflection->isPrivate();
        $array["is_protected"] = $reflection->isProtected();
        $array["is_public"] = $reflection->isPublic();
        $array["is_static"] = $reflection->isStatic();
    }

    private function getClassMethodParameterInfo(&$array, \ReflectionParameter $reflection)
    {
        $array = [];
        $array["name"] = $reflection->getName();
        $array["position"] = $reflection->getPosition();
        $array["passed_by_reference"] = $reflection->isPassedByReference();
        $array["is_optional"] = $reflection->isOptional();
        $array["is_array"] = $reflection->isArray();
        $array["is_default_value_available"] = $reflection->isDefaultValueAvailable();
        $array["is_variadic"] = $reflection->isVariadic();
        if ($reflection->isDefaultValueAvailable()) {
            $array["default_value"] = $reflection->getDefaultValue();
        }
    }
}
