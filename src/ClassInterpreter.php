<?php
namespace AutoDoc;

class ClassInterpreter implements Interpreter
{
    private $classes = [];
    private $existing_classes = [];
    private $classes_queue = [];
    private $classes_failed = [];

    private $evaluated_classes = [];

    public function __construct()
    {
        $this->setExistingItems();
    }

    public function evaluateItems()
    {
        foreach ($this->classes as $class) {
            $this->evaluateItem(
                    $class
                );
        }

        while (!empty($this->classes_queue)) {
            $remaining_pre = count($this->classes_queue);

            foreach ($this->classes_queue as $key => $value) {
                if ($this->evaluateItem($value)) {
                    unset($this->classes_queue[$key]);
                }
            }

            $remaining_post = count($this->classes_queue);

            if ($remaining_pre == $remaining_post) {
                $this->classes_failed = $this->classes_queue;
                $this->classes_queue = [];
            }
        }

        $this->setExistingItems();
    }

    private function evaluateItem($class)
    {
        // print_r($tokens);
        // print_r($namespace);
        // print_r($start);
        // print_r($end);

        $tokens = $class['tokens'];
        $file_name = $class['file_name'];
        $namespace = $class['namespace'];
        $start = $class['class_location_start'];
        $block_start = $class['class_block_start'];
        $end = $class['class_location_end'];

        if (class_exists($namespace."\\".$tokens[$start+2][1])) {
            return true;
        }

        if ($start + 3 < $block_start) {
            $class_dependencies_string = '';

            for ($i = $start + 3; $i <= $block_start - 1; $i++) {
                if (is_array($tokens[$i]) &&
                        ($tokens[$i][0] == T_STRING || $tokens[$i][0] == T_WHITESPACE
                            || $tokens[$i][0] == T_NS_SEPARATOR)) {
                    $class_dependencies_string .= $tokens[$i][1];
                }
            }

            $class_dependencies_string = trim($class_dependencies_string);
            $class_dependencies_string = preg_replace("/\s+/", '|', $class_dependencies_string);
            $class_dependencies = explode('|', $class_dependencies_string);

            foreach ($class_dependencies as $class_dependency) {
                if ($class_dependency && !class_exists($class_dependency)) {
                    if (in_array($class, $this->classes_queue) === false) {
                        array_push($this->classes_queue, $class);
                    }
                    return false;
                }
            }
        }

        $php_code = '';

        for ($i = $start; $i <= $end; $i++) {
            if (is_array($tokens[$i])) {
                $php_code .= $tokens[$i][1];
            } else {
                $php_code .= $tokens[$i];
            }
        }

        if ($namespace) {
            eval("namespace $namespace; $php_code");
        } else {
            eval($php_code);
        }

        $number_of_lines = substr_count($php_code, "\n");

        array_push($this->evaluated_classes, array(
                'name' => $namespace."\\".$tokens[$start+2][1],
                'file_name' => $file_name,
                'start_line' => $tokens[$start][2],
                'end_line' => $tokens[$start][2] + $number_of_lines
            ));

        return true;
    }

    public function setExistingItems()
    {
        $this->existing_classes = array_filter(
                get_declared_classes(),
                function ($className) {
                    return !call_user_func(
                        array(new \ReflectionClass($className), 'isInternal')
                    );
                }
            );
    }

    public function getExistingItems()
    {
        return $this->existing_classes;
    }

    public function setItems($classes)
    {
        $this->classes = $classes;
    }

    public function getItems()
    {
        return $this->classes;
    }

    public function getEvaluatedItems()
    {
        return $this->evaluated_classes;
    }

    public function getFailedItems()
    {
        return $this->classes_failed;
    }

    public function getReflections()
    {
        $class_reflections = [];
        $evaluated_class_reflections = [];

        if (!empty($this->evaluated_classes)) {
            foreach ($this->evaluated_classes as $class) {
                $class_name = ltrim($class['name'], '\\');
                try {
                    $class_reflection = new ClassDocument($class['name']);
                    $class_reflection->setFileName($class['file_name']);
                    $class_reflection->setStartLine($class['start_line']);
                    $class_reflection->setEndLine($class['end_line']);

                    array_push($class_reflections, $class_reflection);
                    array_push($evaluated_class_reflections, $class_name);
                } catch (\ReflectionException $e) {
                    array_push($this->classes_failed, $class_name);
                    trigger_error("class $class_name not found");
                }
            }
        }

        $remaining_classes = array_diff($this->getExistingItems(), $evaluated_class_reflections);

        if (!empty($remaining_classes)) {
            foreach ($remaining_classes as $class) {
                try {
                    $class_reflection = new ClassDocument($class);
                    array_push($class_reflections, $class_reflection);
                } catch (\ReflectionException $e) {
                    array_push($this->classes_failed, $class);
                    trigger_error("class $class not found");
                }
            }
        }

        return $class_reflections;
    }
}
