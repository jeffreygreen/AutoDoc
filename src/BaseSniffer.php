<?php
namespace AutoDoc;

class BaseSniffer implements Sniffer
{
    const NO_ITEM_FOUND = "no_item_found";

    const CLASS_FOUND = "class_found";
    const IN_CLASS = "in_class";

    const FUNCTION_FOUND = "function_found";
    const IN_FUNCTION = "in_function";

    protected $dir = '';

    public function __construct($dir = '.')
    {
        $this->dir = $dir;
    }

    public function search($folder, $pattern)
    {
        $dir = new \RecursiveDirectoryIterator($folder);
        $ite = new \RecursiveIteratorIterator($dir);
        $files = new \RegexIterator($ite, $pattern, \RegexIterator::GET_MATCH);
        $fileList = array();
        foreach ($files as $file) {
            $fileList = array_merge($fileList, $file);
        }
        return $fileList;
    }

    public static function isClassDeclaration($tokens, $i)
    {
        return $tokens[$i - 2][0] == T_CLASS
            && $tokens[$i - 1][0] == T_WHITESPACE
            && $tokens[$i][0] == T_STRING;
    }

    public static function isFunctionDeclaration($tokens, $i)
    {
        return $tokens[$i - 2][0] == T_FUNCTION
            && $tokens[$i - 1][0] == T_WHITESPACE
            && $tokens[$i][0] == T_STRING;
    }

    public static function isNamespaceDeclaration($tokens, $i)
    {
        return $tokens[$i - 2][0] == T_NAMESPACE
            && $tokens[$i - 1][0] == T_WHITESPACE
            && $tokens[$i][0] == T_STRING;
    }
}
