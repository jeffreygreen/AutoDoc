<?php
namespace AutoDoc;

class HTMLPageBuilder
{
    private $file;

    public function __construct($file)
    {
        $this->file = $file;
    }

    public function build($body)
    {
        $header = file_get_contents(__DIR__.'/views/header.html');
        $footer = file_get_contents(__DIR__.'/views/footer.html');
        $file_contents = $header.$body.$footer;
        file_put_contents($this->file, $file_contents);
    }

    public static function arrayDetails($array)
    {
        $details = '';
        foreach ($array as $key => $value) {
            if (!empty($value)) {
                $key_pretty = ucwords(str_replace('_', ' ', $key));
                $value_pretty = strval($value);
                $details .= "<p>$key_pretty: $value_pretty</p>";
            }
        }
        return $details;
    }
}
