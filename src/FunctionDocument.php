<?php
namespace AutoDoc;

class FunctionDocument extends \ReflectionFunction
{
    private $fileName;
    private $startLine;
    private $endLine;

    public function getFileName()
    {
        return $this->fileName;
    }

    public function getStartLine()
    {
        return $this->startLine;
    }
        
    public function getEndLine()
    {
        return $this->endLine;
    }
        
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    public function setStartLine($startLine)
    {
        $this->startLine = $startLine;
    }
        
    public function setEndLine($endLine)
    {
        $this->endLine = $endLine;
    }
}
