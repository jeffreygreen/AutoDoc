<?php
namespace AutoDoc;

class AutoDoc
{
    const DEPTH_CLASSES = "depth_to_scan_classes";
    const DEPTH_FUNCTIONS = "depth_to_scan_functions";
    const DEPTH_VERBOSE = "depth_to_scan_verbose";

    public static function run($dir = '.', $format = ResultFormatter::JSON, $depth = AutoDoc::DEPTH_CLASSES)
    {
        $result = [];

        if ($depth == AutoDoc::DEPTH_CLASSES ||
                $depth == AutoDoc::DEPTH_VERBOSE) {
            $result['classes'] = AutoDoc::documentClasses($dir);
        }

        if ($depth == AutoDoc::DEPTH_FUNCTIONS ||
                $depth == AutoDoc::DEPTH_VERBOSE) {
            $result['functions'] = AutoDoc::documentFunctions($dir);
        }

        $formatted_result = ResultFormatter::format($result, $format);

        return $formatted_result;
    }

    private static function documentClasses($dir)
    {
        $ci = new ClassInterpreter();
        $cs = new ClassSniffer();

        $classes = $cs->sniff($dir);

        $ci->setItems($classes);
        $ci->evaluateItems();

        $d = new ClassDocumenter($ci->getReflections());

        $classes = $d->document();

        return $classes;
    }

    private static function documentFunctions($dir)
    {
        $fi = new FunctionInterpreter();
        $fs = new FunctionSniffer();

        $functions = $fs->sniff($dir);

        $fi->setItems($functions);
        $fi->evaluateItems();

        $f = new FunctionDocumenter($fi->getReflections());

        $functions = $f->document();

        return $functions;
    }
}
