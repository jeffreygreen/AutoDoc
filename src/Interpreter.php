<?php
namespace AutoDoc;

interface Interpreter
{
    public function evaluateItems();

    public function setExistingItems();

    public function getExistingItems();

    public function setItems($items);

    public function getItems();

    public function getEvaluatedItems();

    public function getFailedItems();

    public function getReflections();
}
