<?php
namespace AutoDoc;

class ResultFormatter
{
    const JSON = "json";
    const RAW = "raw";
    const XML = "xml";
    const HTML = "html";

    public static function format($array, $format)
    {
        if ($format == ResultFormatter::JSON) {
            return json_encode($array);
        } elseif ($format == ResultFormatter::RAW) {
            return array_map('strval', $array);
        } elseif ($format == ResultFormatter::XML) {
            return XMLBuilder::build($array);
        } elseif ($format == ResultFormatter::HTML) {
            $htmlSiteBuilder = new HTMLSiteBuilder($array);
            $htmlSiteBuilder->arrayToHTML();

            return $htmlSiteBuilder->getDir();
        } else {
            return null;
        }
    }
}
