<?php
namespace AutoDoc;

class HTMLSiteBuilder
{
    private $array;
    private $dir;

    public function __construct($array)
    {
        $this->array = $array;
        $this->dir = "./.autodoc.tmp.html.".time();
    }

    public function arrayToHTML()
    {
        if (($writable = $this->checkDirLocation()) !== true) {
            return;
        }

        $stylesheet = file_get_contents(__DIR__.'/views/style.css');
        file_put_contents($this->dir.'/style.css', $stylesheet);

        if ($this->array != null) {
            $htmlPageBuilder = new HTMLPageBuilder($this->dir.'/index.html');
            $file_contents = "<h1>AutoDoc</h1><p>This documentation will include:</p><ul>";
            if (array_key_exists('classes', $this->array) && $this->array['classes'] != null) {
                $file_contents .= '<li><a href="classes.html">Classes</a></li>';
                $this->classesToHTML($this->array['classes']);
            }
            if (array_key_exists('functions', $this->array) && $this->array['functions'] != null) {
                $file_contents .= '<li><a href="functions.html">Functions</a></li>';
                $this->functionsToHTML($this->array['functions']);
            }
            $file_contents .= "</ul><p>Please feel free to edit these yourself.</p>";
            $htmlPageBuilder->build($file_contents);
        }
    }

    private function classesToHTML($classes)
    {
        $htmlPageBuilder = new HTMLPageBuilder($this->dir.'/classes.html');
        $file_contents = "<h1>AutoDoc</h1><p>This classes included are:</p><ul>";
        foreach ($classes as $class) {
            $class_name = $class['name'];
            $class_name_html = str_replace('\\', '.', $class['name']);
            $file_contents .= "<li><a href=\"class.$class_name_html.html\">$class_name</a></li>";
            $this->classToHTML($class);
        }
        $file_contents .= "</ul><p>Please feel free to edit these yourself.</p>";
        $htmlPageBuilder->build($file_contents);
    }

    private function classToHTML($class)
    {
        $class_name = $class['name'];
        $class_name_html = str_replace('\\', '.', $class['name']);
        $htmlPageBuilder = new HTMLPageBuilder($this->dir."/class.$class_name_html.html");
        $file_contents = "<h1>Class: $class_name</h1><p>This classes is made up like this:</p><ul>";
        // TODO: Spit out details about this class
        $file_contents .= HTMLPageBuilder::arrayDetails($class);
        $file_contents .= "</ul><p>Please feel free to edit these yourself.</p>";
        $htmlPageBuilder->build($file_contents);
    }

    private function functionsToHTML($functions)
    {
        $htmlPageBuilder = new HTMLPageBuilder($this->dir.'/functions.html');
        $file_contents = "<h1>Functions</h1><p>The functions included are:</p><ul>";
        foreach ($functions as $function) {
            $function_name = $function['name'];
            $function_name_html = str_replace('\\', '.', $function['name']);
            $file_contents .= "<li><a href=\"function.$function_name_html.html\">$function_name</a></li>";
            $this->functionToHTML($function);
        }
        $file_contents .= "</ul><p>Please feel free to edit these yourself.</p>";
        $htmlPageBuilder->build($file_contents);
    }

    private function functionToHTML($function)
    {
        $function_name = $function['name'];
        $function_name_html = str_replace('\\', '.', $function['name']);
        $htmlPageBuilder = new HTMLPageBuilder($this->dir."/function.$function_name_html.html");
        $file_contents = "<h1>Function: $function_name</h1>";
        // TODO: Spit out details about this class
        $file_contents .= HTMLPageBuilder::arrayDetails($function);
        $file_contents .= "</ul><p>Please feel free to edit these yourself.</p>";
        $htmlPageBuilder->build($file_contents);
    }

    private function checkDirLocation()
    {
        if (file_exists($this->dir)) {
            if (!is_dir($this->dir)) {
                return "dir exists and is not dir";
            }
            if (!is_writable($this->dir)) {
                return "dir is not writable";
            }
            $handle = opendir($this->dir);
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    return "dir is not empty";
                }
            }
        } else {
            mkdir($this->dir);
        }
        return true;
    }

    public function getDir()
    {
        return $this->dir;
    }
}
