<?php
namespace AutoDoc;

class FunctionSniffer extends BaseSniffer
{
    public function sniff($dir = null)
    {
        if (is_null($dir)) {
            $dir = $this->dir;
        }

        if (is_null($dir)) {
            return [];
        }


        $code = [];

        $files = $this->search($dir, "/.+\.php/");

        foreach ($files as $file) {
            $file = realpath($file);
            $contents = file_get_contents($file);
            $tokens = token_get_all($contents);

            $namespace = '';

            $sniffer_status = BaseSniffer::NO_ITEM_FOUND;

            $curly_brace_count = 0;

            $function_location_start = 0;

            for ($i = 0; $i < count($tokens); $i++) {
                if ($sniffer_status == BaseSniffer::CLASS_FOUND
                        || $sniffer_status == BaseSniffer::IN_CLASS) {
                    if ($sniffer_status == BaseSniffer::IN_CLASS) {
                        if ($tokens[$i] == '{') {
                            $curly_brace_count++;
                        } elseif ($tokens[$i] == '}') {
                            $curly_brace_count--;

                            if ($curly_brace_count == 0) {
                                $sniffer_status = BaseSniffer::NO_ITEM_FOUND;
                            }
                        }
                    } else {
                        if ($tokens[$i] == '{') {
                            $sniffer_status = BaseSniffer::IN_CLASS;
                            $curly_brace_count++;
                        }
                    }
                } elseif ($sniffer_status == BaseSniffer::FUNCTION_FOUND
                                || $sniffer_status == BaseSniffer::IN_FUNCTION) {
                    if ($sniffer_status == BaseSniffer::IN_FUNCTION) {
                        if ($tokens[$i] == '{') {
                            $curly_brace_count++;
                        } elseif ($tokens[$i] == '}') {
                            $curly_brace_count--;
                            if ($curly_brace_count == 0) {
                                $sniffer_status = BaseSniffer::NO_ITEM_FOUND;
                                $function_location_end = $i;
                                array_push($code, array(
                                                    "tokens" => $tokens,
                                                    "file_name" => $file,
                                                    "namespace" => $namespace,
                                                    "function_location_start" => $function_location_start,
                                                    "function_location_end" => $function_location_end
                                                    ));
                            }
                        }
                    } else {
                        if ($tokens[$i] == '{') {
                            $sniffer_status = BaseSniffer::IN_FUNCTION;
                            $curly_brace_count++;
                        }
                    }
                } else {
                    if ($i >= 2) {
                        // echo "no_class_found\n";
                        if ($this->isNamespaceDeclaration($tokens, $i)) {
                            $namespace = $tokens[$i][1];
                        } elseif ($this->isClassDeclaration($tokens, $i)) {
                            $sniffer_status = BaseSniffer::CLASS_FOUND;
                        } elseif ($this->isFunctionDeclaration($tokens, $i)) {
                            $sniffer_status = BaseSniffer::FUNCTION_FOUND;
                            $function_location_start = $i - 2;
                        }
                    }
                }
            }
        }

        return $code;
    }
}
