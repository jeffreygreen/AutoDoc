<?php
namespace AutoDoc;

class FunctionInterpreter implements Interpreter
{
    private $functions = [];
    private $existing_functions = [];
    private $functions_failed = [];

    private $evaluated_functions = [];

    public function __construct()
    {
        $this->setExistingItems();
    }

    public function evaluateItems()
    {
        foreach ($this->functions as $function) {
            $this->evaluateItem(
                    $function
                );
        }
        $this->setExistingItems();
    }

    private function evaluateItem($function)
    {
        $tokens = $function['tokens'];
        $file_name = $function['file_name'];
        $namespace = $function['namespace'];
        $start = $function['function_location_start'];
        $end = $function['function_location_end'];

        if (function_exists($namespace."\\".$tokens[$start+2][1])) {
            return true;
        }

        $php_code = '';

        for ($i = $start; $i <= $end; $i++) {
            if (is_array($tokens[$i])) {
                $php_code .= $tokens[$i][1];
            } else {
                $php_code .= $tokens[$i];
            }
        }

        if ($namespace) {
            eval("namespace $namespace; $php_code");
        } else {
            eval($php_code);
        }

        $number_of_lines = substr_count($php_code, "\n");

        array_push($this->evaluated_functions, array(
                'name' => $namespace."\\".$tokens[$start+2][1],
                'file_name' => $file_name,
                'start_line' => $tokens[$start][2],
                'end_line' => $tokens[$start][2] + $number_of_lines
            ));

        return true;
    }

    public function setExistingItems()
    {
        $defined_functions = get_defined_functions();
        $this->existing_functions = array_filter(
                $defined_functions['user'],
                function ($functionName) {
                    return !call_user_func(
                        array(new \ReflectionFunction($functionName), 'isInternal')
                    );
                }
            );
    }

    public function getExistingItems()
    {
        return $this->existing_functions;
    }

    public function setItems($functions)
    {
        $this->functions = $functions;
    }

    public function getItems()
    {
        return $this->functions;
    }

    public function getEvaluatedItems()
    {
        return $this->evaluated_functions;
    }

    public function getFailedItems()
    {
        return $this->functions_failed;
    }

    public function getReflections()
    {
        $function_reflections = [];
        $evaluated_function_reflections = [];

        if (!empty($this->evaluated_functions)) {
            foreach ($this->evaluated_functions as $function) {
                $function_name = ltrim($function['name'], '\\');
                try {
                    $function_reflection = new FunctionDocument($function_name);
                    $function_reflection->setFileName($function['file_name']);
                    $function_reflection->setStartLine($function['start_line']);
                    $function_reflection->setEndLine($function['end_line']);

                    array_push($function_reflections, $function_reflection);
                    array_push($evaluated_function_reflections, strtolower($function_name));
                } catch (\ReflectionException $e) {
                    array_push($this->functions_failed, $function_name);
                    trigger_error("function $function_name not found");
                }
            }
        }

        $remaining_functions = array_diff($this->getExistingItems(), $evaluated_function_reflections);

        if (!empty($remaining_functions)) {
            foreach ($remaining_functions as $function) {
                try {
                    $function_reflection = new FunctionDocument($function);
                    array_push($function_reflections, $function_reflection);
                } catch (\ReflectionException $e) {
                    array_push($this->functions_failed, $function);
                    trigger_error("function $function not found");
                }
            }
        }

        return $function_reflections;
    }
}
