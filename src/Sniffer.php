<?php
namespace AutoDoc;

interface Sniffer
{
    public function search($folder, $pattern);
}
