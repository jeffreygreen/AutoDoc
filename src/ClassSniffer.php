<?php
namespace AutoDoc;

class ClassSniffer extends BaseSniffer
{
    public function sniff($dir = null)
    {
        if (is_null($dir)) {
            $dir = $this->dir;
        }

        if (is_null($dir)) {
            return [];
        }

        $code = [];

        $files = $this->search($dir, "/.+\.php/");

        foreach ($files as $file) {
            $file = realpath($file);
            $contents = file_get_contents($file);
            $tokens = token_get_all($contents);

            $namespace = '';

            $sniffer_status = BaseSniffer::NO_ITEM_FOUND;

            $curly_brace_count = 0;

            $class_location_start = 0;
            $class_block_start = 0;

            for ($i = 0; $i < count($tokens); $i++) {
                if ($sniffer_status == BaseSniffer::CLASS_FOUND
                        || $sniffer_status == BaseSniffer::IN_CLASS) {
                    if ($sniffer_status == BaseSniffer::IN_CLASS) {
                        if ($tokens[$i] == '{') {
                            $curly_brace_count++;
                        } elseif ($tokens[$i] == '}') {
                            $curly_brace_count--;

                            if ($curly_brace_count == 0) {
                                $sniffer_status = BaseSniffer::NO_ITEM_FOUND;
                                $class_location_end = $i;
                                array_push($code, array(
                                                    "tokens" => $tokens,
                                                    "file_name" => $file,
                                                    "namespace" => $namespace,
                                                    "class_location_start" => $class_location_start,
                                                    "class_block_start" => $class_block_start,
                                                    "class_location_end" => $class_location_end
                                                ));
                            }
                        }
                    } else {
                        if ($tokens[$i] == '{') {
                            $sniffer_status = BaseSniffer::IN_CLASS;
                            $curly_brace_count++;
                            $class_block_start = $i;
                        }
                    }
                } else {
                    if ($i >= 2) {
                        if ($this->isNamespaceDeclaration($tokens, $i)) {
                            $namespace = $tokens[$i][1];
                        } elseif ($this->isClassDeclaration($tokens, $i)) {
                            $sniffer_status = BaseSniffer::CLASS_FOUND;
                            $class_location_start = $i - 2;
                        }
                    }
                }
            }
        }

        return $code;
    }
}
