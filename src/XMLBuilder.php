<?php
namespace AutoDoc;

class XMLBuilder
{
    public static function build($array)
    {
        $xml_root = new \SimpleXMLElement('<?xml version="1.0"?><root></root>');
        XMLBuilder::arrayToXML($array, $xml_root);
        return $xml_root->asXML();
    }

    private static function arrayToXML($array, \SimpleXMLElement &$xml_root)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (is_numeric($key) === false) {
                    if (strpos($key, '\\')) {
                        $key = str_replace('\\', '.', $key);
                    }
                    $subNode = $xml_root->addChild("$key");
                    XMLBuilder::arrayToXML($value, $subNode);
                } else {
                    $subNode = $xml_root->addChild("item$key");
                    XMLBuilder::arrayToXML($value, $subNode);
                }
            } else {
                if (is_numeric($key) === false) {
                    $xml_root->addChild("$key", htmlspecialchars("$value"));
                } else {
                    $xml_root->addChild("item$key", htmlspecialchars("$value"));
                }
            }
        }
    }
}
