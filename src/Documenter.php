<?php
namespace AutoDoc;

interface Documenter
{
    public function document();
}
